const Holiday = require("./holiday");

const zh = Holiday.list.tc.vcalendar[0].vevent;
const en = Holiday.list.en.vcalendar[0].vevent;
const sc = Holiday.list.sc.vcalendar[0].vevent;

module.exports = {
  listAll: () => {
    let holidayArray = [];
    for (let i = 0; i < zh.length; i++) {
      const holidayObj = {
        date: "",
        desZH: "",
        desSC: "",
        desEN: ""
      };
      holidayObj.date = new Date(
        `${zh[i].dtstart[0].substr(0, 4)}-${zh[i].dtstart[0].substr(4, 2)}-${zh[
          i
        ].dtstart[0].substr(6, 2)}`
      );
      holidayObj.desZH = zh[i].summary;
      holidayObj.desSC = sc[i].summary;
      holidayObj.desEN = en[i].summary;
      holidayArray.push(holidayObj);
    }
    return holidayArray;
  },
  isHoliday: date => {
    // Date Param Must be Date Object
    if (!isValidDateObj(date)) {
      return false;
    }
    const convertedDate = convertDate(date);
    const holidayList = module.exports.listAll();
    if (
      holidayList.filter(i => convertDate(i.date) === convertedDate).length >
        0 ||
      date.getDay() === 0
    ) {
      return true;
    }

    return false;
  },
  getHolidayInfo: date => {
    if (!isValidDateObj(date)) {
      return null;
    }
    const convertedDate = convertDate(date);
    const holidayList = module.exports.listAll();
    const filteredList = holidayList.filter(
      i => convertDate(i.date) === convertedDate
    );
    if (filteredList.length > 0) {
      return filteredList[0];
    }
    return null;
  }
};

const isValidDateObj = date =>
  date &&
  Object.prototype.toString.call(date) === "[object Date]" &&
  !isNaN(date);

const convertDate = date =>
  date
    .toISOString()
    .slice(0, 10)
    .replace(/-/g, "");
