# Hong Kong Holiday

- List of Hong Kong Holiday between 2018-2020.
- Determine is the day is holiday or not.
- Show Hong Kong Holiday Information.

![](https://img.shields.io/github/stars/pandao/editor.md.svg) ![](https://img.shields.io/github/forks/pandao/editor.md.svg) ![](https://img.shields.io/github/tag/pandao/editor.md.svg) ![](https://img.shields.io/github/release/pandao/editor.md.svg) ![](https://img.shields.io/github/issues/pandao/editor.md.svg) ![](https://img.shields.io/bower/v/editor.md.svg)

## Install

```bash
#NPM
npm i holiday-hk --save
```

## Example

```javascript
const holidayHK = require("holiday-hk");

// Fetch all Hong Kong Holiday list
holidayHK.listAll(); // Return Holiday List [{date: Date, desEn: String, desZH: String, desSc: String}...]

// Determine Is the day is holiday or not
const xmas = new Date("2020-12-25"); // Xams
holidayHK.isHoliday(xmas); // Retrun true

const sunday = new Date("2020-03-15"); // Sunday
holidayHK.isHoliday(sunday); // Retrun false, Because it is Sunday

const workday = new Date("2020-12-28"); // Workday
holidayHK.isHoliday(workday); // Retrun false, Because it is Workday

const xmas = new Date("2020-12-25"); // Xams
holidayHK.getHolidayInfo(xmas); // Return {date: Date, desEn: String, desZH: String, desSc: String}
```

## Method

| Method           | Return Type |
| ---------------- | ----------- |
| listAll()        | Array       |
| isHoliday()      | Boolean     |
| getHolidayInfo() | Object      |

## Properties

| Property | Type   | Example                  |
| -------- | ------ | ------------------------ |
| date     | Date   | 2020-12-26T00:00:00.000Z |
| desEn    | String | Christmas Day            |
| desZh    | String | 聖誕節                   |
| desSc    | String | 圣诞节                   |

---

## License

MIT

## ChangeLog

## End

Free Hong Kong, Revolution now\
Five demands, not one less
