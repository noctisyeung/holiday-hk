const { expect } = require("chai");
const { listAll, isHoliday, getHolidayInfo } = require("../index");

describe("List Date Module", () => {
  it("Should Return Array", () => {
    expect(listAll()).to.be.an("array");
  });
  it("Check All Key In Object", () => {
    listAll().forEach(i => {
      expect(i).to.have.all.deep.keys("date", "desEN", "desSC", "desZH");
    });
  });
});

describe("Check is Holiday Module", () => {
  it("Check is Holiday", () => {
    const date = new Date("2020-12-25");
    expect(isHoliday(date)).to.be.true;
  });
  it("Check is Sunday", () => {
    const date = new Date("2020-03-15");
    expect(isHoliday(date)).to.be.true;
  });
  it("Check Isn't Holiday", () => {
    const date = new Date("2020-12-28");
    expect(isHoliday(date)).to.be.false;
  });
});

describe("Get Holiday Info Module", () => {
  it("Get Holiday Info", () => {
    const date = new Date("2020-12-25");
    expect(getHolidayInfo(date)).to.deep.equal({
      date: date,
      desZH: "聖誕節",
      desSC: "圣诞节",
      desEN: "Christmas Day"
    });
  });
  it("Get Not Found Holiday Info", () => {
    const date = new Date("2020-12-29");
    expect(getHolidayInfo(date)).to.equal(null);
  });
});
